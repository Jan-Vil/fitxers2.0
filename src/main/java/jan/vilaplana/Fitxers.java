package jan.vilaplana;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.UserPrincipal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Fitxers {

    //<editor-fold desc="JAVA IO">
    public void escriuFitxerTextIO(
            String fitxer,
            String text,
            boolean afegir
    ) {
        try (
                FileWriter out = new FileWriter(fitxer, afegir)) // true → afegim al fitxer
        {
            out.write(text + "\n"); // escrivim al fitxer
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public void escriuFitxerTextLlistaIO(
            String fitxer,
            List<String> text,
            boolean afegir) {

        for (String linia : text) {
            escriuFitxerTextIO(fitxer, linia, afegir);
        }
    }
    public String retornaFitxerTextIO(String fitxer) {

        String linia; // per recollir la línia
        String text = "";

        try {
            // BLOC DE TRY .. CATCH
            // NECESSARI PER UTILITZAR STREAMS
            FileReader in = new FileReader(fitxer);
            {
                Scanner input = new Scanner(in);
                while (input.hasNextLine()) { // Mentre hi hagen línies a l'arxiu ...
                    linia = input.nextLine(); // Agafa una línia
                    text = text + linia + "\n"; // guardem les dades de l’usuari
                }
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        return text;
    }
    public List<String> retornaFitxerTextLlistaIO(String fitxer) {

        String linia; // per recollir la línia
        List<String> text = new ArrayList<>();        // contingut del fitxer
        try {
            // BLOC DE TRY .. CATCH
            // NECESSARI PER UTILITZAR STREAMS
            FileReader in = new FileReader(fitxer);
            {
                Scanner input = new Scanner(in);
                while (input.hasNextLine()) { // Mentre hi hagen línies a l'arxiu ...
                    linia = input.nextLine(); // Agafa una línia
                    text.add(linia); // guardem les dades de l’usuari
                }
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        return text;
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="ADMINISTRACIÓ FITXERS IO">
    public String obteRutaIO(String fitxer) {
        File file = new File(fitxer);
        return file.getAbsolutePath();
    }
    public boolean esDirectoriIO(String directori) {
        File file = new File(directori);
        return file.isDirectory();
    }
    public void renombraFitxerIO(String fitxer, String nouNom) {
        java.io.File file = new java.io.File(fitxer);
        java.io.File file2 = new java.io.File(nouNom);
        if (file.renameTo(file2)) {
            System.out.println("Fitxer renombrat: " + file.getName() + " a " + file2.getName());
        } else {
            System.out.println("No s'ha pogut renombrar el fitxer.");
        }
    }
    public void llistaFitxersIO(String directori) {
        File file = new File(directori);
        String[] fitxers = file.list();
        for (String fitxer : fitxers) {
            System.out.println(fitxer);
        }
    }
    public boolean existeixIO(String ruta){
        File arxiu = new File(ruta);
        return arxiu.exists();
    }
    public void crearDirectoriIO(String directori){
        File arxiu = new File(directori);
        arxiu.mkdir();
    }
    public void eliminarFitxerDirectoriI0(String ruta){
        File arxiu = new File(ruta);
        arxiu.delete();
    }
    public void moureFitxerDirectoriIO(String fitxerOriginal, String fitxerDesti){
        File fOriginal = new File(fitxerOriginal);
        File fDesti = new File(fitxerDesti);
        fOriginal.renameTo(fDesti);
    }
    public boolean existeix(String ruta){
        Path rutaFitxer = Paths.get(ruta);
        boolean existeix = Files.exists(rutaFitxer);
        return existeix;
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="JAVA NIO">
    public void creaDirectori(String ruta) throws IOException {
        Path path = Paths.get(ruta);         // Path --> import java.nio.file.Path;
        // Paths --> import java.nio.file.Paths
        Files.createDirectory(path);    // Files --> import java.nio.file.Files
    }
    public String propìetariFitxer (String fitx) throws IOException{
        Path path = Paths.get(fitx);
        // propietari
        UserPrincipal propietari = Files.getOwner(path);

        return propietari.toString();
    }
    public void eliminarFitxerDirectori(String fitx) throws IOException{
        Path ruta = Paths.get(fitx);
        Files.delete(ruta);
    }
    public void copiarFitxerDirectori(String origen, String desti) throws IOException {
        Path rutaOrigen = Paths.get(origen);
        Path rutaDesti = Paths.get(desti);

        Files.copy(rutaOrigen,rutaDesti);
    }
    public void moureFitxerDirectori(String origen, String desti) throws IOException {
        Path rutaOrigen = Paths.get(origen);
        Path rutaDesti = Paths.get(desti);

        Files.move(rutaOrigen,rutaDesti);
    }


    //</editor-fold>

    //<editor-fold desc="FITXERS BINARIS">

    public void escriuFitxerBinari(Object obj, String nomFitxer, boolean afegir) throws IOException {
        if (!afegir||!existeix(nomFitxer)){
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(nomFitxer, afegir));
            out.writeObject(obj);
        } else {
            MeuObjectOutputStream out2 = new MeuObjectOutputStream(new FileOutputStream(nomFitxer, afegir));
            out2.writeObject(obj);
        }
    }

    public List<Object> retornaFitxerObjecteEnLlista(String nomFitxer){
        List <Object>lObjs = new ArrayList<>();
        try{
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(nomFitxer));
            do{
                Object obj = in.readObject();
                lObjs.add(obj);
            } while (in!=null);
            in.close();
            in=null;
       } catch (Exception e){
            e.toString();
        }
       return lObjs;

    }
    //</editor-fold>

}