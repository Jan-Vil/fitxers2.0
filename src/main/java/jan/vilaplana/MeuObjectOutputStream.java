package jan.vilaplana;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

public class MeuObjectOutputStream extends ObjectOutputStream {
    public MeuObjectOutputStream(OutputStream out) throws IOException {

        super(out);
    }
    protected MeuObjectOutputStream() throws IOException, SecurityException {

        super();
    }

    protected void writeStreamHeader() throws IOException{
        // no hi ha codi
    }
}
