package jan.vilaplana.Classes;

import jan.vilaplana.Fitxers;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Llibre {

    private static final Fitxers f=new Fitxers();
    private static final String dir=".dat";
    private static final String nomFitxer=dir+"/fitxers.dat";


    //<editor-fold desc="Propietats">
    private String ISBN;
    private String titol;
    private String autor;
    //</editor-fold>

    //<editor-fold desc="Constructors">


    public Llibre() {
    }

    public Llibre(String ISBN, String titol, String autor) {
        this.ISBN = ISBN;
        this.titol = titol;
        this.autor = autor;
    }

    //</editor-fold>

    //<editor-fold desc="Getters">

    public String getISBN() {
        return ISBN;
    }

    public String getTitol() {
        return titol;
    }

    public String getAutor() {
        return autor;
    }

    public String getFitxer() {
        return nomFitxer;
    }


    //</editor-fold>


    //<editor-fold desc="Mètodes">

    public void guardaLlibre()  {

        try {
            if (!f.existeix(dir))
                f.creaDirectori(dir);

            f.escriuFitxerBinari(this, nomFitxer, true);
        }catch (Exception e){
            System.err.println("Hi ha hagut algun error en l'escriptura");
        }

    }
    public List<Llibre> retornaLlibres() throws FileNotFoundException,
        InterruptedException, ClassNotFoundException{
        Fitxers f=new Fitxers();
        List<Object>objs=f.retornaFitxerObjecteEnLlista(nomFitxer);
        List<Llibre>llibres=converteixALlibre(objs);
        return llibres;
    }

    private List<Llibre> converteixALlibre(List<Object> lObjectes){
        List<Llibre> llibres=new ArrayList<>();
        Llibre lli = new Llibre();

        for (Object obj: lObjectes) {
            lli = (Llibre) obj;
            llibres.add(lli);
        }
        return(llibres);
    }


    //</editor-fold>


}
